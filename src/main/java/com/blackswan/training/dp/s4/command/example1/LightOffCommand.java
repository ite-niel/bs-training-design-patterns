package com.blackswan.training.dp.s4.command.example1;

//Concrete Command
public class LightOffCommand implements Command {
    //reference to the light
    Light light;

    public LightOffCommand(Light light) {
        this.light = light;
    }

    public void execute() {
        System.out.println("COMMAND - LIGHT OFF EXECUTING");
        light.switchOff();
    }
}