package com.blackswan.training.dp.s1.strategy.example3;

/**
 * Abstrack Class for the Duck implementation.
 */
public abstract class Duck {

    private FlyBehaviour flyBehaviour;
    private QuackBehaviour quackBehaviour;

    // Default Constructor.
    public Duck(FlyBehaviour flyBehaviour, QuackBehaviour quackBehaviour) {
        this.flyBehaviour = flyBehaviour;
        this.quackBehaviour = quackBehaviour;
    }

    /**
     * Display the duck.
     */
    public abstract void display();

    /**
     * Execute the performFly Behaviour.
     */
    public void performFly() {
        flyBehaviour.fly();
    }

    /**
     * Execute the performQuack Behaviour.
     */
    public void performQuack() {
        quackBehaviour.quack();
    }

    /**
     * Swim - because all ducks can swim.
     */
    public void swim() {
        System.out.println("All ducks float, even decoys!");
    }
}
