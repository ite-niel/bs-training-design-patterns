package com.blackswan.training.dp.s1.strategy.example3;

/**
 * Interface for the Quack Behavior.
 */
public interface QuackBehaviour {

    void quack();
}
