package com.blackswan.training.dp.s2.observer.example1;

import com.blackswan.training.dp.s2.observer.Util;

public class Cnn {
    private NewsFeed theFeed;

    public Cnn(NewsFeed theFeed) {
        System.out.println("This, is CNN...");
        this.theFeed = theFeed;
        startPoller();
    }

    private void startPoller() {
        new Thread(() -> {
            // Check the feed very 5 seconds for an update
            while (true) {
                Util.sleep(3000L);
                displayNewsTicker();
            }
        }).start();
    }

    private void displayNewsTicker() {
        System.out.println("CNN news feed: " + theFeed.getState());
    }
}
