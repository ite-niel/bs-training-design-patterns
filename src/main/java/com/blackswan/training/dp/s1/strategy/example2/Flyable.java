package com.blackswan.training.dp.s1.strategy.example2;

/**
 * Flyable Interface.
 */
public interface Flyable {

    void fly();
}
