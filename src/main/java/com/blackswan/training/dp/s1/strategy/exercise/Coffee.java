package com.blackswan.training.dp.s1.strategy.exercise;

/**
 * Refactor the Coffee class to implement the strategy pattern.
 */
public class Coffee {

    String type;

    Coffee(String type) {
        this.type = type;
    }

    public void brew() {
        switch (type) {
            case "Latte":
                // Milk Preparation
                System.out.println("Steam a lot of Milk");
                // Coffee Preparation
                System.out.println("Add Coffee and Water");
                // Brew
                System.out.println("Brew with high pressure");
                // Pour
                System.out.println("Use Tall Glass");
                break;
            case "Espresso":
                // Milk Preparation
                System.out.println("No Milk");
                // Coffee Preparation
                System.out.println("Add only coffee");
                // Brew
                System.out.println("Brew with high pressure");
                // Pour
                System.out.println("Pour in espresso glass");
                break;
            case "Cappuccino":
                // Milk Preparation
                System.out.println("Steam Milk");
                // Coffee Preparation
                System.out.println("Add Coffee and Water");
                // Brew
                System.out.println("Brew with high pressure");
                // Pour
                System.out.println("Pour in cup");
                break;
            case "Americano":
                // Milk Preparation
                System.out.println("Pour Milk");
                // Coffee Preparation
                System.out.println("Add Coffee and Water");
                // Brew
                System.out.println("Use Plunger");
                // Pour
                System.out.println("Pour in cup");
                break;
            default:
                System.out.println("Couldn't fine the coffee type " + type);
        }
    }
}
