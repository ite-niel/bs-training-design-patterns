package com.blackswan.training.dp.s1.strategy.example3;

/**
 * FlyNoWay implementation.
 */
public class FlyNoWay implements FlyBehaviour {

    @Override
    public void fly() {
        System.out.println("I can't fly");
    }
}
