package com.blackswan.training.dp.s1.strategy.example3;

/**
 * Rubber Duck implementation.
 */
public class RubberDuck extends Duck {

    public RubberDuck() {
        super(new FlyNoWay(), new Quack());
    }

    @Override
    public void display() {
        System.out.println("I'm a real RubberDuck duck");
        performQuack();
        performFly();
        swim();
    }
}
