package com.blackswan.training.dp.s1.strategy.exercise;

/**
 * Refactor the Latte class to implement the strategy pattern.
 */
public class Latte extends Coffee {
    Latte() {
        super("Latte");
    }
}
