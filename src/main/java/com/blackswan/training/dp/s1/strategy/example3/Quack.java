package com.blackswan.training.dp.s1.strategy.example3;

/**
 * Quack Behaviour implementation.
 */
public class Quack implements QuackBehaviour {
    @Override
    public void quack() {
        System.out.println("Quack");
    }
}
