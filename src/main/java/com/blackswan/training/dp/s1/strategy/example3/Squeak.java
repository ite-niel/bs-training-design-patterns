package com.blackswan.training.dp.s1.strategy.example3;

/**
 * Squeak implementation for the Quack Behavior.
 */
public class Squeak implements QuackBehaviour {

    @Override
    public void quack() {
        System.out.println("Squeak");
    }
}
