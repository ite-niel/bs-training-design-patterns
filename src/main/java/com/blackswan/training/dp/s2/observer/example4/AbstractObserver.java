package com.blackswan.training.dp.s2.observer.example4;

public abstract class AbstractObserver implements MyObserver {
    MySubject observable;

    public void setObservable(MySubject observable) {
        this.observable = observable;
    }
}
