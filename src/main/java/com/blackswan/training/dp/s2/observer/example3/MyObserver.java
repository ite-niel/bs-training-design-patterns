package com.blackswan.training.dp.s2.observer.example3;

/**
 * Something that observes, or listens to an observable / subject.
 */
public interface MyObserver {
    void update(String data);
}
