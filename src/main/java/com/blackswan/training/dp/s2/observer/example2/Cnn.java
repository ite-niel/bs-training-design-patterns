package com.blackswan.training.dp.s2.observer.example2;

import com.blackswan.training.dp.s2.observer.Util;

public class Cnn {
    NewsFeed theFeed;

    public void update() {
        System.out.println("CNN news feed: " + theFeed.getState());
    }
}
