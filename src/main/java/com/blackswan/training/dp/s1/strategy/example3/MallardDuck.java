package com.blackswan.training.dp.s1.strategy.example3;

/**
 * Mallard Duck Implementation.
 */
public class MallardDuck extends Duck {

    public MallardDuck() {
        super(new FlyWithWings(), new Quack());
    }

    @Override
    public void display() {
        System.out.println("I'm a real Mallard duck");
        performQuack();
        performFly();
        swim();
    }
}
