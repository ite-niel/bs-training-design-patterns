package com.blackswan.training.dp.s1.strategy.example2;

/**
 * Mallard Duck implementation.
 */
public class MallardDuck extends Duck implements Quackable, Flyable {

    @Override
    public void fly() {
        System.out.println("I can also Fly");
    }

    @Override
    public void quack() {
        System.out.println("I can also Quack");
    }

    @Override
    public void display() {
        super.display();
        System.out.println("I'm a Mallard Duck");
        this.fly();
        this.quack();
    }
}
