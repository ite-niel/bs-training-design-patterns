package com.blackswan.training.dp.s1.strategy.example3;

/**
 * Mute Quack behaviour implementation.
 */
public class MuteQuack implements QuackBehaviour {

    @Override
    public void quack() {
        System.out.println("<< Silence >>");
    }
}
