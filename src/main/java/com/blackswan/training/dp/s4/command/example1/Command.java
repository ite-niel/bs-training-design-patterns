package com.blackswan.training.dp.s4.command.example1;

//Command
public interface Command {
    public void execute();
}