package com.blackswan.training.dp.s1.strategy.example2;

/**
 * Rubber Duck implementation.
 */
public class RubberDuck extends Duck implements Quackable {
    @Override
    public void quack() {
        System.out.println("I can also quack");
    }

    @Override
    public void display() {
        super.display();
        System.out.println("I'm also a Rubber Duck");
        this.quack();
    }
}
