package com.blackswan.training.dp.s1.strategy.exercise;

/**
 * Refactor the Cappuccino class to implement the strategy pattern.
 */
public class Cappuccino extends Coffee {
    Cappuccino() {
        super("Cappuccino");
    }
}
