package com.blackswan.training.dp.s2.observer.example1;

import com.blackswan.training.dp.s2.observer.Util;

import java.util.Observable;
import java.util.Observer;

public class Main {
    public static void main(String[] args) {
        // Create the news feed
        NewsFeed feed = new NewsFeed();

        // Setup channels
        Cnn cnn = new Cnn(feed);
        Bbc bbc = new Bbc(feed);

        // Loop 3 times and update news story
        for (int i = 0; i < 3; i++) {
            Util.sleep(5000L);
            feed.updateFeed("NEWS STORY " + (i + 1));
        }

        // Done
        System.out.println("DONE");
        System.exit(0);
    }
}
