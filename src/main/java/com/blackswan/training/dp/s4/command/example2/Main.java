package com.blackswan.training.dp.s4.command.example2;

public class Main {
    public static void main(String[] args) {
        Robot robot = new Robot();
        RobotController controller = new RobotController();

        MoveCommand move = new MoveCommand(robot, 1000);
        controller.add(move);

        RotateLeftCommand rotate = new RotateLeftCommand(robot, 45);
        controller.add(rotate);

        TakeSampleCommand scoop = new TakeSampleCommand(robot);
        controller.add(scoop);

        System.out.println("GO FOR IT!");
        controller.executeCommands();

        System.out.println("OH NO - I NEED TO UNDO THE STUFF ABOVE");
        controller.undoCommands();
    }
}
