package com.blackswan.training.dp.s1.strategy.example2;

/**
 * Class implementation with interfaces.
 */
public class Client {

    /**
     * This example of the client should show the difficulty in implementing new methods where Classes doesn't necessarily need the implementation.
     *
     * Problem: Duplication of code - the interfaces' implementations are an issue.
     *
     * @param args
     */
    public static void main(String[] args) {
        // Let's create 2 implementations that should work:
        Duck mallardDuck = new MallardDuck();
        System.out.println("What is this duck?");
        mallardDuck.display();

        Duck redheadDuck = new RedheadDuck();
        System.out.println("What is this duck?");
        redheadDuck.display();

        // But now we have the problem with Duck's that can't implement the super class' methods.

        Duck rubberDuck = new RubberDuck();
        System.out.println("What is this duck?");
        rubberDuck.display();

        Duck decoyDuck = new DecoyDuck();
        System.out.println("What is this duck?");
        decoyDuck.display(); // THIS IS Fixed not


    }
}
