package com.blackswan.training.dp.s4.command.example1;


// Receiver
public class Light {
    private boolean on;

    public void switchOn() {
        System.out.println("LIGHT: ON");
        on = true;
    }

    public void switchOff() {
        System.out.println("LIGHT: OFF");
        on = false;
    }
}
