package com.blackswan.training.dp.s1.strategy.exercise;

/**
 * Refactor the Americano class to implement the strategy pattern.
 */
public class Americano extends Coffee {

    Americano() {
        super("Americano");
    }
}
