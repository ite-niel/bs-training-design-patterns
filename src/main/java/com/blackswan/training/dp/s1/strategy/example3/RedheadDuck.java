package com.blackswan.training.dp.s1.strategy.example3;

/**
 * Redhead Duck implementation.
 */
public class RedheadDuck extends Duck {

    public RedheadDuck() {
        super(new FlyWithWings(), new Quack());
    }

    @Override
    public void display() {
        System.out.println("I'm a real RedheadDuck duck");
        performQuack();
        performFly();
    }
}
