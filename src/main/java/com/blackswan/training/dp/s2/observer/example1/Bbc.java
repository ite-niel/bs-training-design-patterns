package com.blackswan.training.dp.s2.observer.example1;

import com.blackswan.training.dp.s2.observer.Util;

public class Bbc {
    private NewsFeed theFeed;

    public Bbc(NewsFeed theFeed) {
        System.out.println("Welcome to BBC!");
        this.theFeed = theFeed;
        startPoller();
    }

    private void startPoller() {
        new Thread(() -> {
            // Check the feed very 5 seconds for an update
            while (true) {
                Util.sleep(3000L);
                displayNewsTicker();
            }
        }).start();
    }

    private void displayNewsTicker() {
        System.out.println("BBC news feed: " + theFeed.getState());
    }
}
