package com.blackswan.training.dp.s2.observer.example4;

public interface MySubject {
    void addObserver(MyObserver observer);
    void removeObserver(MyObserver observer);
    void notifyObservers();
    String getData();
}
