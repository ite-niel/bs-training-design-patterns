package com.blackswan.training.dp.s1.strategy.example3;

/**
 * Decoy Duck implementation.
 */
public class DecoyDuck extends Duck {

    public DecoyDuck() {
        super(new FlyNoWay(), new MuteQuack());
    }

    @Override
    public void display() {
        System.out.println("I'm a real DecoyDuck duck");
        performQuack();
        performFly();
        swim();
    }
}