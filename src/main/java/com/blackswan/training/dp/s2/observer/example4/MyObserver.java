package com.blackswan.training.dp.s2.observer.example4;

public interface MyObserver {
    void setObservable(MySubject observable);
    void update();
}
