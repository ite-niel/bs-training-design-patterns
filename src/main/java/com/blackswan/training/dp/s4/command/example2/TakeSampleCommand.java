package com.blackswan.training.dp.s4.command.example2;

public class TakeSampleCommand implements RobotCommand {
    private Robot robot;

    public TakeSampleCommand(Robot robot) {
        this.robot = robot;
    }

    @Override
    public void execute() {
        robot.takeSample(true);
    }

    @Override
    public void undo() {
        robot.takeSample(false);
    }
}
