package com.blackswan.training.dp.s2.observer.example3;

import com.blackswan.training.dp.s2.observer.Util;

public class Main {
    public static void main(String[] args) {
        // Create news feed and news channels
        NewsFeed feed = new NewsFeed();
        Bbc bbc = new Bbc();
        Cnn cnn = new Cnn();

        // Add CNN and BBC as observers to the newsfeed
        feed.addObserver(cnn);
        feed.addObserver(bbc);

        // Wait a bit then fire off first news story
        Util.sleep(3000L);
        feed.updateNews("NEWS STORY 1");

        // Next news story
        Util.sleep(3000L);
        feed.updateNews("NEWS STORY 2");

        // REMOVE CNN
        feed.removeObserver(cnn);

        // Last news story - only BBC is listening
        Util.sleep(3000L);
        feed.updateNews("NEWS STORY 3 - should only see one now");

        System.out.println("DONE");
        System.exit(0);
    }
}
