package com.blackswan.training.dp.s4.command.example1;


//Concrete Command
public class LightOnCommand implements Command {
    //reference to the light
    Light light;

    public LightOnCommand(Light light) {
        this.light = light;
    }

    public void execute() {
        System.out.println("COMMAND - LIGHT ON EXECUTING");
        light.switchOn();
    }
}
