package com.blackswan.training.dp.s1.strategy.exercise;

/**
 * Refactor the Espresso class to implement the strategy pattern.
 */
public class Espresso extends Coffee {
    Espresso() {
        super("Espresso");
    }
}
