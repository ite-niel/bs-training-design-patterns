package com.blackswan.training.dp.s4.command.example2;

public interface RobotCommand {
    void execute();
    void undo();
}
