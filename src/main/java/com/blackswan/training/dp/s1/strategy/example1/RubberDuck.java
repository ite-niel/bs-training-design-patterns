package com.blackswan.training.dp.s1.strategy.example1;

/**
 * Rubber Duck implementation.
 */
public class RubberDuck extends Duck {

    @Override
    public void display() {
        System.out.println("I'm A Rubber Duck");
        super.display();
    }
}
