package com.blackswan.training.dp.s1.strategy.example1;

/**
 * Redhead Duck implementation.
 */
public class RedheadDuck extends Duck {

    @Override
    public void display() {
        System.out.println("I'm a Redhead Duck");
        super.display();
    }
}
