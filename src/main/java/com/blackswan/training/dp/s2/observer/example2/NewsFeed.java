package com.blackswan.training.dp.s2.observer.example2;

/**
 * Updated news of the day.
 */
public class NewsFeed {
    private String theFeed;
    private Bbc bbc;
    private Cnn cnn;

    public NewsFeed(Cnn cnn, Bbc bbc) {
        bbc.theFeed = this;
        cnn.theFeed = this;
        this.bbc = bbc;
        this.cnn = cnn;
    }

    public void updateFeed(String updatedFeed) {
        System.out.println("****  Updating feed: " + updatedFeed + " ****");
        this.theFeed = updatedFeed;
        bbc.update();
        cnn.update();
    }

    public String getState() {
        return theFeed;
    }
}
