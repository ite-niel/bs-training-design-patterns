package com.blackswan.training.dp.s4.command.example2;

public class MoveCommand implements RobotCommand {
    private Robot robot;
    private int forwardDistance;

    public MoveCommand(Robot robot, int distance) {
        this.robot = robot;
        this.forwardDistance = distance;
    }

    @Override
    public void execute() {
        robot.move(forwardDistance);
    }

    @Override
    public void undo() {
        robot.move(-forwardDistance);
    }
}
