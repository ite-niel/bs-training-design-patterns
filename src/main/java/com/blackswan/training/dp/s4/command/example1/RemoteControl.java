package com.blackswan.training.dp.s4.command.example1;

//Invoker
public class RemoteControl {
    private Command command;

    public void setCommand(Command command) {
        this.command = command;
    }

    public void pressButton() {
        System.out.println("REMOTE: Press Button");
        command.execute();
    }
}
