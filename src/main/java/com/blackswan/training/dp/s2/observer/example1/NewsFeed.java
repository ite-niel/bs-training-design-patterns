package com.blackswan.training.dp.s2.observer.example1;

/**
 * Updated news of the day.
 */
public class NewsFeed {
    private String theFeed = "N/A";

    public void updateFeed(String updatedFeed) {
        System.out.println("****  Updating feed: " + updatedFeed + " ****");
        this.theFeed = updatedFeed;
    }

    public String getState() {
        return theFeed;
    }
}
