package com.blackswan.training.dp.s1.strategy.example3;

/**
 * Fly Behaviour interface.
 */
public interface FlyBehaviour {
    void fly();
}
