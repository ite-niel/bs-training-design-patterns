package com.blackswan.training.dp.s2.observer.example4;

public class Cnn extends AbstractObserver {
    @Override
    public void update() {
        System.out.println("CNN news:  " + observable.getData());
    }
}
