package com.blackswan.training.dp.s1.strategy.example1;

/**
 * Mallard Duck implementation.
 */
public class MallardDuck extends Duck {

    @Override
    public void display() {
        System.out.println("I'm A Mallard Duck");
        super.display();
    }
}
