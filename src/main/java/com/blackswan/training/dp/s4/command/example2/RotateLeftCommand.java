package com.blackswan.training.dp.s4.command.example2;

public class RotateLeftCommand implements RobotCommand {
    private Robot robot;
    private double angle;

    public RotateLeftCommand(Robot robot, double angle) {
        this.robot = robot;
        this.angle = angle;
    }

    @Override
    public void execute() {
        robot.rotateLeft(angle);
    }

    @Override
    public void undo() {
        robot.rotateRight(angle);
    }
}
