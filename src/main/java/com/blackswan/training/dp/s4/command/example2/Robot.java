package com.blackswan.training.dp.s4.command.example2;

public class Robot
{
    public void move(int distance)
    {
        if (distance > 0) {
            System.out.printf("Robot moved forwards %smm.\n", distance);
        } else {
            System.out.printf("Robot moved backwards %smm.\n", -distance);
        }
    }

    public void rotateLeft(double angle)
    {
        if (angle > 0) {
            System.out.printf("Robot rotated left %s degrees.\n", angle);
        }
        else {
            System.out.printf("Robot rotated right %s degrees.\n", -angle);
        }
    }

    public void rotateRight(double angle)
    {
        if (angle > 0) {
            System.out.printf("Robot rotated right %s degrees.\n", angle);
        }
        else {
            System.out.printf("Robot rotated left %s degrees.\n", -angle);
        }
    }

    public void takeSample(boolean take)
    {
        if(take) {
            System.out.printf("Robot took sample\n");
        }
        else {
            System.out.printf("Robot released sample\n");
        }
    }
}
