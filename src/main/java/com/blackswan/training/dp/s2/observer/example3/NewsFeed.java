package com.blackswan.training.dp.s2.observer.example3;

import java.util.ArrayList;
import java.util.List;

public class NewsFeed implements MySubject {
    // Keeps track of the news data itself
    private String theNews = "N/A";

    // Keeps track of all observers
    final List<MyObserver> observers = new ArrayList<>();

    @Override
    public void addObserver(MyObserver channel) {
        System.out.println("Add channel " + channel.getClass().getSimpleName());
        observers.add(channel);
    }

    @Override
    public void removeObserver(MyObserver channel) {
        System.out.println("Removing channel " + channel.getClass().getSimpleName());
        observers.remove(channel);
    }

    @Override
    public void notifyObservers() {
        // Effectively PUSHING data to the observers
        observers.forEach(channel -> channel.update(theNews));
    }

    /**
     * Updates the news in the feed and then notifies all the observers (observers)
     */
    public void updateNews(String updatedFeed) {
        System.out.println("****  Updating feed: " + updatedFeed + " - PUSHING to observers ****");
        this.theNews = updatedFeed;
        notifyObservers();
    }
}
