package com.blackswan.training.dp.s1.strategy.example1;

/**
 * Client implementation for solution using extends.
 */
public class Client {

    /**
     * This example of the client should show the difficulty in implementing new methods where Classes doesn't necessarily need the implementation.
     *
     * Problem: Changes can unintentionally affect other ducks. Localised udate to code caused a non-local side affect (flying rubber ducks).
     *
     * @param args
     */
    public static void main(String[] args) {
        // Let's create 2 implementations that should work:
        Duck mallardDuck = new MallardDuck();
        Duck redheadDuck = new RedheadDuck();

        System.out.println("What is this duck?");
        mallardDuck.display();
        System.out.println("What is this duck?");
        redheadDuck.display();

        // But now we have the problem with Duck's that can't implement the super class' methods.

        Duck rubberDuck = new RubberDuck();
        System.err.println("What is this duck?");
        rubberDuck.display(); // THIS IS AN ISSUE!
    }
}
