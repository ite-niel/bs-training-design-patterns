package com.blackswan.training.dp.s2.observer.example3;

public class Bbc implements MyObserver {
    @Override
    public void update(String data) {
        System.out.println("BBC news:  " + data);
    }
}
