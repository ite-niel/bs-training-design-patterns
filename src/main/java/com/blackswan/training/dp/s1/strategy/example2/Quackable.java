package com.blackswan.training.dp.s1.strategy.example2;

/**
 * Quackable Interface.
 */
public interface Quackable {

    void quack();
}
