package com.blackswan.training.dp.s1.strategy.exercise;

/**
 * The Black Swan Coffee Shop Simulator
 */
public class Client {

    public static void main(String[] args) {

        System.out.println("Welcome to the Black Swan Coffee Shop Simulator");

        Coffee cappuccino = new Cappuccino();
        System.out.println("One Cappuccino coming up");
        cappuccino.brew();
        System.out.println();

        Coffee espresso = new Espresso();
        System.out.println("One Espresso coming up");
        espresso.brew();
        System.out.println();

        Coffee americano = new Americano();
        System.out.println("One Americano coming up");
        americano.brew();

    }
}
