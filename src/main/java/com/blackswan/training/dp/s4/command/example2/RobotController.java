package com.blackswan.training.dp.s4.command.example2;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class RobotController {
    Queue<RobotCommand> commands = new LinkedList<>();
    Stack<RobotCommand> undoStack = new Stack<>();

    public RobotController add(RobotCommand command) {
        commands.add(command);
        return this;
    }

    public void executeCommands() {
        while (commands.size() > 0) {
            final RobotCommand currentCommand = commands.poll();
            currentCommand.execute();
            undoStack.push(currentCommand);
        }
    }

    public void undoCommands() {
        while (undoStack.size() > 0) {
            final RobotCommand currentCommand = undoStack.pop();
            currentCommand.undo();
        }
    }
}
