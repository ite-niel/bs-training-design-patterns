package com.blackswan.training.dp.s2.observer.example2;

import com.blackswan.training.dp.s2.observer.Util;

public class Bbc {
    NewsFeed theFeed;

    public void update() {
        System.out.println("BBC news feed: " + theFeed.getState());
    }
}
