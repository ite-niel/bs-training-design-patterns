package com.blackswan.training.dp.s1.strategy.example1;

/**
 * Duck class to encapsulate the Duck behavior.
 *
 * @author Niel Koekemoer
 */
public abstract class Duck {

    /**
     * Quack behavior.
     */
    public void quack() {
        System.out.println("I'm Quacking");
    }

    /**
     * Swim behavior.
     */
    public void swim() {
        System.out.println("I'm Swimming");
    }

    /**
     * Swim behavior.
     */
    public void fly() {
        System.out.println("I'm Flying");
    }

    /**
     * Display behavior.
     */
    public void display() {
        this.quack();
        this.swim();
        this.fly();
    }
}
