package com.blackswan.training.dp.s2.observer.example2;

import com.blackswan.training.dp.s2.observer.Util;

import java.util.Observable;

public class Main {
    public static void main(String[] args) {
        // Setup news channels
        Cnn cnn = new Cnn();
        Bbc bbc = new Bbc();

        // Setup news feed and tell it to which channels to send updates
        NewsFeed feed = new NewsFeed(cnn, bbc);

        // Loop 3 times and update news story
        for (int i = 0; i < 3; i++) {
            Util.sleep(3000L);
            feed.updateFeed("NEWS STORY " + (i + 1));
        }

        // Done
        System.out.println("DONE");
        System.exit(0);
    }
}
