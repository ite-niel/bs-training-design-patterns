package com.blackswan.training.dp.s4.command.example2;

public class RotateRightCommand implements RobotCommand {
    private Robot robot;
    private double angle;

    public RotateRightCommand(Robot robot, double angle) {
        this.robot = robot;
        this.angle = angle;
    }

    @Override
    public void execute() {
        robot.rotateRight(angle);
    }

    @Override
    public void undo() {
        robot.rotateLeft(angle);
    }
}
