package com.blackswan.training.dp.s2.observer.example3;

/**
 * This is an "Observable" - something that can be observed, or listened to for updates.
 */
public interface MySubject {
    void addObserver(MyObserver observer);
    void removeObserver(MyObserver observer);
    void notifyObservers();
}
