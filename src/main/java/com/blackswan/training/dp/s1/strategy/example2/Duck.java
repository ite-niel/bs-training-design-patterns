package com.blackswan.training.dp.s1.strategy.example2;

/**
 * Abstract Duck implementation.
 */
public abstract class Duck {

    public void swim(){
        System.out.println("I'm swimming");
    }

    public void display(){
        System.out.println("I'm a duck");
        this.swim();
    }
}
