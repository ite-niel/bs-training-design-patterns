package com.blackswan.training.dp.s1.strategy.example3;

/**
 * FlyNoWings Implementation.
 */
public class FlyWithWings implements FlyBehaviour {

    @Override
    public void fly() {
        System.out.println("I'm flying");
    }
}
