package com.blackswan.training.dp.s2.observer.example4;

public class Bbc extends AbstractObserver {
    @Override
    public void update() {
        System.out.println("BBC news:  " + observable.getData());
    }
}
